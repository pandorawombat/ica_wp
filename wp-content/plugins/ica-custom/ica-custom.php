<?php
/*
Plugin Name: ICA Custom Plugin
Plugin URI: 
Description: 
Version: 1.0.0
Author: Jenny Chalek
Author URI: 
License: GPLv2
*/

function acf_google_map_api($api){
	
	$api['key'] = 'AIzaSyC6zG8H3r6o-aRpt1f0zOC9ZayGL2ORc6c';
	
	return $api;
	
}
add_filter('acf/fields/google_map/api', 'acf_google_map_api');

function modify_read_more_link() {
    return '<a class="more-link" href="' . get_permalink() . '">Read More</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );