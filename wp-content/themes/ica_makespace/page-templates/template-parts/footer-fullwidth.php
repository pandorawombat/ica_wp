<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package some_like_it_neat
 */
?>

		<?php tha_content_bottom(); ?>

		</main><!-- #main -->

		<?php tha_content_after(); ?>

		<?php tha_footer_before(); ?>

		<?php tha_footer_after(); ?>

	</div><!-- .wrap -->

</div><!-- #page -->

<?php tha_body_bottom(); ?>

<?php $marker_img = wp_upload_dir()['baseurl']."/2016/08/markersm.png"; ?>

<?php wp_footer(); ?>

<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6zG8H3r6o-aRpt1f0zOC9ZayGL2ORc6c"></script>-->
<script src="https://use.typekit.net/ubj0cuf.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<script type="text/javascript">
jQuery(function($) {
    
    function resizeStuff(){
        if($(window).width() > 769){
            var lft = $(window).width() - 710;
        } else {
            var lft = $(window).width() - 250;
        }
        
        //calculate and position the triangle mask (can't just use a polygon because not compatible with IE
        var imght = $('#ica-img').height();
        var imgwd = $('#ica-img').width();
        var triht = imght * .1656184486373165;
        var tripos = $('#ica-img').position();
        var triverpos = tripos.top + ((imght - triht - 5) / 2);
        var trihorpos = imgwd - triht + 1;
        $('.inset-triangle-left').css('top',triverpos).css('left',trihorpos)
                .css('border-top-width',triht).css('border-bottom-width',triht).css('border-right-width',triht);
        
        if($(window).width() > 1000){
            $('.abouts-wrap.blue-back').css('height',$('#blue-about-img').height());
        } else {
            $('.abouts-wrap.blue-back').css('height','100%');
        }
    }
    
    $('.menu-item').click(function(e){
        var current = $(this).find('a');
        $('.menu-item').each(function(){
            $(this).find('a').css('color','#ffffff').css('font-weight','400');
        });
        current.css('color','#03518c').css('font-weight','700');
    });
    
    $('.more-link').removeAttr('href').text('(READ MORE)');
    $('.more-toggle').click(function(){
        $(this).toggle();
        $('.close-more').toggle();
        $('.toggle-text').toggle();
        setTimeout(resizeStuff, 100);
    });
    $('.close-more').click(function(){
        $(this).toggle();
        $('.more-toggle').toggle();
        $('.toggle-text').toggle();
        setTimeout(resizeStuff, 100);
    });

    setTimeout(resizeStuff, 100);
    setTimeout(makeTheMap, 100);
    $(window).resize(function(){
        resizeStuff();
        makeTheMap();
    });
//    makeTheMap();
    
    $('.menu-top-links-container').prepend($("#menu-icon"));
    
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                    }, 1000);
                return false;
            }
        }
    });
    
    function makeTheMap(){
        var map = new google.maps.Map($('.acf-map')[0], {
          zoom: 17,
          center: {lat: 38.299381, lng: -85.776413},
          scrollwheel: false,
          mapTypeControl: false
        });

        var image = '<?php echo $marker_img; ?>';
        var marker = new google.maps.Marker({
          position: {lat: $('.marker').data('lat'), lng: $('.marker').data('lng')},
          map: map,
          icon: image
        });
        
    }
    
});
</script>

</body>
</html>
