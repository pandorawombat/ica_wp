<?php
/**
 * Template Name: Full-Width, No Sidebars
 *
 * This template display content at full with, with no sidebars.
 * Please note that this is the WordPress construct of pages and that other 'pages' on your WordPress site will use a different template.
 *
 * @package some_like_it_neat
 */

get_template_part( 'page-templates/template-parts/header', 'fullwidth' ); ?>


<div id="primary" class="content-area">
    <?php
        $image = get_field('hero_image');
        if(!empty($image)): ?>
            <!--<img src="<?php //echo $image['url']; ?>" alt="<?php //echo $image['alt']; ?>" />-->
    <?php endif; ?>
    <div class="hero" style="background-image: url('<?php echo $image["url"]; ?>')">
        <div class="ica-logo">
            <?php
            $image = get_field('logo');
            if(!empty($image)): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
        </div>
        <?php $args = array('menu'=>'Top Links','container'=>'nav'); ?>
        <?php wp_nav_menu($args); ?>
        <?php //wp_nav_menu('Top Links','menu','menu-top-links','nav'); ?>
        <h1 id="hero-headline"><?php if(the_field('headline')) the_field('headline'); ?></h1>
    </div>
    
    <div id="services">
        <div class="services">
            
            <div class="service-1">
                    <div class="svc-c-1">
                        <?php
                            $image = get_field('service_1_icon');
                            if(!empty($image)): ?>
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        <?php endif; ?>
                    </div>
                <?php
                    $post_object = get_field('service_1');

                    if($post_object):
                        // override $post
                        $post = $post_object;
                        setup_postdata($post);
                ?>
                    <h4>ICA</h4>
                    <h3><?php the_title(); ?></h3>
                    <div class="post-body"><?php the_content(); ?></div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="service-2">
                    <div class="svc-c-2">
                        <?php
                            $image = get_field('service_2_icon');
                            if(!empty($image)): ?>
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        <?php endif; ?>
                    </div>
                <?php
                    $post_object = get_field('service_2');

                    if($post_object):
                        // override $post
                        $post = $post_object;
                        setup_postdata($post);
                ?>
                    <h4>ICA</h4>
                    <h3><?php the_title(); ?></h3>
                    <div class="post-body"><?php the_content(); ?></div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <div class="service-3">
                <div class="svc-c-3">
                    <?php
                        $image = get_field('service_3_icon');
                        if(!empty($image)): ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endif; ?>
                </div>
                <?php
                    $post_object = get_field('service_3');

                    if($post_object):
                        // override $post
                        $post = $post_object;
                        setup_postdata($post); 
                ?>
                    <h4>ICA</h4>
                    <h3><?php the_title(); ?></h3>
                    <div class="post-body"><?php the_content(); ?></div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
   <div id="about" class="abouts">
        
        <div class="abouts-right indent">
            <?php
                $image = get_field('icon_a1');
                if(!empty($image)): ?>
                    <div class="about-icon">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>
            <?php endif; ?>
            <h4><?php if(the_field('first_head_a1')) the_field('first_head_a1'); ?></h4>
            <h3><?php if(the_field('second_head_a1')) the_field('second_head_a1'); ?></h3>
            <div class="about-body"><?php if(the_field('body_a1')) the_field('body_a1'); ?></div>
        </div>
       <div class="abouts-left"><?php
            $image = get_field('image_left');
            if(!empty($image)): ?>
                <img id="ica-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
                <div class="inset-triangle-left"></div>
        </div>
       
    </div> 
    <div class="row-space"></div>
    <div class="abouts">
        
        <?php $image = get_field('icon_a2'); ?>
        <div class="abouts-wrap blue-back">
            <div class="about-icon">
                <?php if(!empty($image)): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </div>
            <div class="about-heads">
                <h4 class="white"><?php if(the_field('first_head_a2')) the_field('first_head_a2'); ?></h4>
                <h3 class="white"><?php if(the_field('second_head_a2')) the_field('second_head_a2'); ?></h3>
            </div>
            <div class="about-body white"><?php if(the_field('body_a2')) the_field('body_a2'); ?></div>
        </div>
                
        <div class="abouts-right">
            <?php
            $image = get_field('image_right');
            if(!empty($image)): ?>
                <img id="blue-about-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
        </div>
    </div>
    <div class="row-space"></div>
    <div class="map-wrap">
    <?php
        $location = get_field('map');
        if(!empty($location)):
    ?>
        <div class="acf-map"></div>
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
    <?php endif; ?>
        
    <div id="contact" class="contact indent">
        <h4><?php if(the_field('first_head_c')) the_field('first_head_c'); ?></h4>
        <h3><?php if(the_field('second_head_c')) the_field('second_head_c'); ?></h3>
        <p><?php if(the_field('text_1_c')) the_field('text_1_c'); ?></p>
        <p class="semibold"><?php if(the_field('bold_text_1_c')) the_field('bold_text_1_c'); ?></p>
        <h3 class="blue"><?php if(the_field('third_head_c')) the_field('third_head_c'); ?></h3>
        <h4><?php if(the_field('fourth_head_c')) the_field('fourth_head_c'); ?></h4>
        <h3><?php if(the_field('fifth_head_c')) the_field('fifth_head_c'); ?></h3>
        <div class="phone-line">
            <?php
            $image = get_field('icon_1_c');
            if(!empty($image)): ?>
            <span class="phone-icon"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></span>
            <?php endif; ?>
            <span class="body"><?php if(the_field('city_1_c')) the_field('city_1_c'); ?>&nbsp;</span>
            <span class="body bold blue"><?php if(the_field('phone_1_c')) the_field('phone_1_c'); ?>&ensp;</span>
            <?php
            $image = get_field('icon_2_c');
            if(!empty($image)): ?>
            <span class="phone-icon"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></span>
            <?php endif; ?>
            <span class="body"><?php if(the_field('city_2_c')) the_field('city_2_c'); ?>&nbsp;</span>
            <span class="body bold blue"><?php if(the_field('phone_2_c')) the_field('phone_2_c'); ?></span>
        </div>
        <div class="row-space-sm"></div>
        <p><?php if(the_field('text_2_c')) the_field('text_2_c'); ?></p>
        
    </div>
    </div>
    <div class="clearit"></div>

    <div class="row-space"></div>
    <div class="ica-footer blue-back indent">
        <p>© <?php echo date('Y');?> Merrel Bierman Excavating, Inc. <a href="#">Privacy Policy</a> | <a href="#">Site Map</a> | <a href="#">Site Info</a></p>
        <img id="bee" src="<?php echo wp_upload_dir()['baseurl'] ?>/2016/08/bee.png" alt="MakeSpace Web Design"/>
    </div>

</div><!-- #primary -->

<?php get_template_part( 'page-templates/template-parts/footer', 'fullwidth' ); ?>
<?php $hamburger = wp_upload_dir()['baseurl']."/2016/08/menu-icon.png"; ?>
<a href="#" style="background-image: url('<?php echo $hamburger; ?>')" id="menu-icon"></a>